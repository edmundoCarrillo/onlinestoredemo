package com.onlineStore.service;

import java.util.List;

import com.onlineStore.domain.Usuario;

public interface UsuarioService {

	public List<Usuario> findAll();

}
