package com.onlineStore.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onlineStore.domain.Usuario;
import com.onlineStore.service.UsuarioService;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/public/v1/usuarios")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Usuario> retriveAllUsers() {
		return usuarioService.findAll();
	}

}
